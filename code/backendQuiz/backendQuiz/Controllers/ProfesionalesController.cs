﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuiz;

namespace backendQuiz.Controllers
{
    public class ProfesionalesController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Profesionales
        public IQueryable<profesionales> Getprofesionales()
        {
            return db.profesionales;
        }

        // GET: api/Profesionales/5
        [ResponseType(typeof(profesionales))]
        public IHttpActionResult Getprofesionales(int id)
        {
            profesionales profesionales = db.profesionales.Find(id);
            if (profesionales == null)
            {
                return NotFound();
            }

            return Ok(profesionales);
        }

        // PUT: api/Profesionales/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putprofesionales(int id, profesionales profesionales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != profesionales.commission_margin)
            {
                return BadRequest();
            }

            db.Entry(profesionales).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!profesionalesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Profesionales
        [ResponseType(typeof(profesionales))]
        public IHttpActionResult Postprofesionales(profesionales profesionales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.profesionales.Add(profesionales);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (profesionalesExists(profesionales.commission_margin))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = profesionales.commission_margin }, profesionales);
        }

        // DELETE: api/Profesionales/5
        [ResponseType(typeof(profesionales))]
        public IHttpActionResult Deleteprofesionales(int id)
        {
            profesionales profesionales = db.profesionales.Find(id);
            if (profesionales == null)
            {
                return NotFound();
            }

            db.profesionales.Remove(profesionales);
            db.SaveChanges();

            return Ok(profesionales);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool profesionalesExists(int id)
        {
            return db.profesionales.Count(e => e.commission_margin == id) > 0;
        }
    }
}