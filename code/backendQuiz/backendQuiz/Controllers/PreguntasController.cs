﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuiz;

namespace backendQuiz.Controllers
{
    public class PreguntasController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Preguntas
        public IQueryable<preguntas> Getpreguntas()
        {
            return db.preguntas;
        }

        // GET: api/Preguntas/5
        [ResponseType(typeof(preguntas))]
        public IHttpActionResult Getpreguntas(int id)
        {
            preguntas preguntas = db.preguntas.Find(id);
            if (preguntas == null)
            {
                return NotFound();
            }

            return Ok(preguntas);
        }

        // PUT: api/Preguntas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putpreguntas(int id, preguntas preguntas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != preguntas.id)
            {
                return BadRequest();
            }

            db.Entry(preguntas).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!preguntasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Preguntas
        [ResponseType(typeof(preguntas))]
        public IHttpActionResult Postpreguntas(preguntas preguntas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.preguntas.Add(preguntas);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = preguntas.id }, preguntas);
        }

        // DELETE: api/Preguntas/5
        [ResponseType(typeof(preguntas))]
        public IHttpActionResult Deletepreguntas(int id)
        {
            preguntas preguntas = db.preguntas.Find(id);
            if (preguntas == null)
            {
                return NotFound();
            }

            db.preguntas.Remove(preguntas);
            db.SaveChanges();

            return Ok(preguntas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool preguntasExists(int id)
        {
            return db.preguntas.Count(e => e.id == id) > 0;
        }
    }
}