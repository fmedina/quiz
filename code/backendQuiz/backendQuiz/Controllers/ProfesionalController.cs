﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuiz;

namespace backendQuiz.Controllers
{
    public class ProfesionalController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Profesional
        public IQueryable<profesional> Getprofesional()
        {
            return db.profesional;
        }

        // GET: api/Profesional/5
        [ResponseType(typeof(profesional))]
        public IHttpActionResult Getprofesional(int id)
        {
            profesional profesional = db.profesional.Find(id);
            if (profesional == null)
            {
                return NotFound();
            }

            return Ok(profesional);
        }

        // PUT: api/Profesional/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putprofesional(int id, profesional profesional)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != profesional.id)
            {
                return BadRequest();
            }

            db.Entry(profesional).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!profesionalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Profesional
        [ResponseType(typeof(profesional))]
        public IHttpActionResult Postprofesional(profesional profesional)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.profesional.Add(profesional);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = profesional.id }, profesional);
        }

        // DELETE: api/Profesional/5
        [ResponseType(typeof(profesional))]
        public IHttpActionResult Deleteprofesional(int id)
        {
            profesional profesional = db.profesional.Find(id);
            if (profesional == null)
            {
                return NotFound();
            }

            db.profesional.Remove(profesional);
            db.SaveChanges();

            return Ok(profesional);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool profesionalExists(int id)
        {
            return db.profesional.Count(e => e.id == id) > 0;
        }
    }
}