﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuiz;

namespace backendQuiz.Controllers
{
    public class TransprincipalController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Transprincipal
        public IQueryable<trans_principal> Gettrans_principal()
        {
            return db.trans_principal;
        }

        // GET: api/Transprincipal/5
        [ResponseType(typeof(trans_principal))]
        public IHttpActionResult Gettrans_principal(int id)
        {
            trans_principal trans_principal = db.trans_principal.Find(id);
            if (trans_principal == null)
            {
                return NotFound();
            }

            return Ok(trans_principal);
        }

        // PUT: api/Transprincipal/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttrans_principal(int id, trans_principal trans_principal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trans_principal.id)
            {
                return BadRequest();
            }

            db.Entry(trans_principal).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trans_principalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Transprincipal
        [ResponseType(typeof(trans_principal))]
        public IHttpActionResult Posttrans_principal(trans_principal trans_principal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.trans_principal.Add(trans_principal);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = trans_principal.id }, trans_principal);
        }

        // DELETE: api/Transprincipal/5
        [ResponseType(typeof(trans_principal))]
        public IHttpActionResult Deletetrans_principal(int id)
        {
            trans_principal trans_principal = db.trans_principal.Find(id);
            if (trans_principal == null)
            {
                return NotFound();
            }

            db.trans_principal.Remove(trans_principal);
            db.SaveChanges();

            return Ok(trans_principal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool trans_principalExists(int id)
        {
            return db.trans_principal.Count(e => e.id == id) > 0;
        }
    }
}