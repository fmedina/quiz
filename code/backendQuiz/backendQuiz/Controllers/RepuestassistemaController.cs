﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuiz;

namespace backendQuiz.Controllers
{
    public class RepuestassistemaController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Repuestassistema
        public IQueryable<repuestas_sistema> Getrepuestas_sistema()
        {
            return db.repuestas_sistema;
        }

        // GET: api/Repuestassistema/5
        [ResponseType(typeof(repuestas_sistema))]
        public IHttpActionResult Getrepuestas_sistema(int id)
        {
            repuestas_sistema repuestas_sistema = db.repuestas_sistema.Find(id);
            if (repuestas_sistema == null)
            {
                return NotFound();
            }

            return Ok(repuestas_sistema);
        }

        // PUT: api/Repuestassistema/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putrepuestas_sistema(int id, repuestas_sistema repuestas_sistema)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != repuestas_sistema.id)
            {
                return BadRequest();
            }

            db.Entry(repuestas_sistema).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!repuestas_sistemaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Repuestassistema
        [ResponseType(typeof(repuestas_sistema))]
        public IHttpActionResult Postrepuestas_sistema(repuestas_sistema repuestas_sistema)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.repuestas_sistema.Add(repuestas_sistema);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = repuestas_sistema.id }, repuestas_sistema);
        }

        // DELETE: api/Repuestassistema/5
        [ResponseType(typeof(repuestas_sistema))]
        public IHttpActionResult Deleterepuestas_sistema(int id)
        {
            repuestas_sistema repuestas_sistema = db.repuestas_sistema.Find(id);
            if (repuestas_sistema == null)
            {
                return NotFound();
            }

            db.repuestas_sistema.Remove(repuestas_sistema);
            db.SaveChanges();

            return Ok(repuestas_sistema);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool repuestas_sistemaExists(int id)
        {
            return db.repuestas_sistema.Count(e => e.id == id) > 0;
        }
    }
}