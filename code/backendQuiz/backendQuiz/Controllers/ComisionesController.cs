﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuiz;

namespace backendQuiz.Controllers
{
    public class ComisionesController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Comisiones
        public IQueryable<comisiones> Getcomisiones()
        {
            return db.comisiones;
        }

        // GET: api/Comisiones/5
        [ResponseType(typeof(comisiones))]
        public IHttpActionResult Getcomisiones(int id)
        {
            comisiones comisiones = db.comisiones.Find(id);
            if (comisiones == null)
            {
                return NotFound();
            }

            return Ok(comisiones);
        }

        // PUT: api/Comisiones/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putcomisiones(int id, comisiones comisiones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != comisiones.id)
            {
                return BadRequest();
            }

            db.Entry(comisiones).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!comisionesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Comisiones
        [ResponseType(typeof(comisiones))]
        public IHttpActionResult Postcomisiones(comisiones comisiones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.comisiones.Add(comisiones);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = comisiones.id }, comisiones);
        }

        // DELETE: api/Comisiones/5
        [ResponseType(typeof(comisiones))]
        public IHttpActionResult Deletecomisiones(int id)
        {
            comisiones comisiones = db.comisiones.Find(id);
            if (comisiones == null)
            {
                return NotFound();
            }

            db.comisiones.Remove(comisiones);
            db.SaveChanges();

            return Ok(comisiones);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool comisionesExists(int id)
        {
            return db.comisiones.Count(e => e.id == id) > 0;
        }
    }
}