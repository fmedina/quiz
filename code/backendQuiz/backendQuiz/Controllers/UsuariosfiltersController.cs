﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuiz;

namespace backendQuiz.Controllers
{
    public class UsuariosfiltersController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Usuariosfilters
        public IQueryable<usuariosfilter> Getusuariosfilter()
        {
            return db.usuariosfilter;
        }

        // GET: api/Usuariosfilters/5
        [ResponseType(typeof(usuariosfilter))]
        public IHttpActionResult Getusuariosfilter(int id)
        {
            usuariosfilter usuariosfilter = db.usuariosfilter.Find(id);
            if (usuariosfilter == null)
            {
                return NotFound();
            }

            return Ok(usuariosfilter);
        }

        // PUT: api/Usuariosfilters/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putusuariosfilter(int id, usuariosfilter usuariosfilter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usuariosfilter.id)
            {
                return BadRequest();
            }

            db.Entry(usuariosfilter).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!usuariosfilterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Usuariosfilters
        [ResponseType(typeof(usuariosfilter))]
        public IHttpActionResult Postusuariosfilter(usuariosfilter usuariosfilter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.usuariosfilter.Add(usuariosfilter);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = usuariosfilter.id }, usuariosfilter);
        }

        // DELETE: api/Usuariosfilters/5
        [ResponseType(typeof(usuariosfilter))]
        public IHttpActionResult Deleteusuariosfilter(int id)
        {
            usuariosfilter usuariosfilter = db.usuariosfilter.Find(id);
            if (usuariosfilter == null)
            {
                return NotFound();
            }

            db.usuariosfilter.Remove(usuariosfilter);
            db.SaveChanges();

            return Ok(usuariosfilter);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool usuariosfilterExists(int id)
        {
            return db.usuariosfilter.Count(e => e.id == id) > 0;
        }
    }
}