﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuiz;

namespace backendQuiz.Controllers
{
    public class EmpresasController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Empresas
        public IQueryable<empresa> Getempresa()
        {
            return db.empresa;
        }

        // GET: api/Empresas/5
        [ResponseType(typeof(empresa))]
        public IHttpActionResult Getempresa(string id)
        {
            empresa empresa = db.empresa.Find(id);
            if (empresa == null)
            {
                return NotFound();
            }

            return Ok(empresa);
        }

        // PUT: api/Empresas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putempresa(string id, empresa empresa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != empresa.name)
            {
                return BadRequest();
            }

            db.Entry(empresa).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!empresaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Empresas
        [ResponseType(typeof(empresa))]
        public IHttpActionResult Postempresa(empresa empresa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.empresa.Add(empresa);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (empresaExists(empresa.name))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = empresa.name }, empresa);
        }

        // DELETE: api/Empresas/5
        [ResponseType(typeof(empresa))]
        public IHttpActionResult Deleteempresa(string id)
        {
            empresa empresa = db.empresa.Find(id);
            if (empresa == null)
            {
                return NotFound();
            }

            db.empresa.Remove(empresa);
            db.SaveChanges();

            return Ok(empresa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool empresaExists(string id)
        {
            return db.empresa.Count(e => e.name == id) > 0;
        }
    }
}