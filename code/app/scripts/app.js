'use strict';

/**
 * @ngdoc overview
 * @name tareas2App
 * @description
 * # tareas2App
 *
 * Main module of the application.
 */
angular
  .module('quizApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/cursos/:idCurso', {
        templateUrl: 'views/cursos.html',
        controller: 'CursosCtrl'
      })      
      .otherwise({
        redirectTo: '/'
      });
  });
