'use strict';

angular.module('quizApp')
	.factory('MainResource', function($resource){
		var factory = {
			getConfig: $resource('http://127.0.0.1/api/Empresa', {}, {
				empresa: {method: 'GET', isArray:true}
			})
		};
		return factory;
	});