'use strict';

/**
 * @ngdoc function
 * @name quizApp.controller:CursosCtrl
 * @description
 * # CursosCtrl
 * Controller of the quizApp
 */
angular.module('quizApp')
  .controller('CursosCtrl', function ($scope,$routeParams) {
    $scope.idCurso = $routeParams.idCurso;
  });
