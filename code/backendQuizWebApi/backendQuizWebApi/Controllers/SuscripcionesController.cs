﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuizWebApi;

namespace backendQuizWebApi.Controllers
{
    public class SuscripcionesController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Suscripciones
        public IQueryable<suscripcion> Getsuscripcion()
        {
            return db.suscripcion;
        }

        // GET: api/Suscripciones/5
        [ResponseType(typeof(suscripcion))]
        public IHttpActionResult Getsuscripcion(int id)
        {
            suscripcion suscripcion = db.suscripcion.Find(id);
            if (suscripcion == null)
            {
                return NotFound();
            }

            return Ok(suscripcion);
        }

        // PUT: api/Suscripciones/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putsuscripcion(int id, suscripcion suscripcion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != suscripcion.id)
            {
                return BadRequest();
            }

            db.Entry(suscripcion).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!suscripcionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Suscripciones
        [ResponseType(typeof(suscripcion))]
        public IHttpActionResult Postsuscripcion(suscripcion suscripcion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.suscripcion.Add(suscripcion);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = suscripcion.id }, suscripcion);
        }

        // DELETE: api/Suscripciones/5
        [ResponseType(typeof(suscripcion))]
        public IHttpActionResult Deletesuscripcion(int id)
        {
            suscripcion suscripcion = db.suscripcion.Find(id);
            if (suscripcion == null)
            {
                return NotFound();
            }

            db.suscripcion.Remove(suscripcion);
            db.SaveChanges();

            return Ok(suscripcion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool suscripcionExists(int id)
        {
            return db.suscripcion.Count(e => e.id == id) > 0;
        }
    }
}