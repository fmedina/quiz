﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuizWebApi;

namespace backendQuizWebApi.Controllers
{
    public class TranspagosController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Transpagos
        public IQueryable<trans_pagos> Gettrans_pagos()
        {
            return db.trans_pagos;
        }

        // GET: api/Transpagos/5
        [ResponseType(typeof(trans_pagos))]
        public IHttpActionResult Gettrans_pagos(int id)
        {
            trans_pagos trans_pagos = db.trans_pagos.Find(id);
            if (trans_pagos == null)
            {
                return NotFound();
            }

            return Ok(trans_pagos);
        }

        // PUT: api/Transpagos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttrans_pagos(int id, trans_pagos trans_pagos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trans_pagos.id)
            {
                return BadRequest();
            }

            db.Entry(trans_pagos).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trans_pagosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Transpagos
        [ResponseType(typeof(trans_pagos))]
        public IHttpActionResult Posttrans_pagos(trans_pagos trans_pagos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.trans_pagos.Add(trans_pagos);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = trans_pagos.id }, trans_pagos);
        }

        // DELETE: api/Transpagos/5
        [ResponseType(typeof(trans_pagos))]
        public IHttpActionResult Deletetrans_pagos(int id)
        {
            trans_pagos trans_pagos = db.trans_pagos.Find(id);
            if (trans_pagos == null)
            {
                return NotFound();
            }

            db.trans_pagos.Remove(trans_pagos);
            db.SaveChanges();

            return Ok(trans_pagos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool trans_pagosExists(int id)
        {
            return db.trans_pagos.Count(e => e.id == id) > 0;
        }
    }
}