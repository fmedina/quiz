﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuizWebApi;

namespace backendQuizWebApi.Controllers
{
    public class ExamanesController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Examanes
        public IQueryable<examanes> Getexamanes()
        {
            return db.examanes;
        }

        // GET: api/Examanes/5
        [ResponseType(typeof(examanes))]
        public IHttpActionResult Getexamanes(int id)
        {
            examanes examanes = db.examanes.Find(id);
            if (examanes == null)
            {
                return NotFound();
            }

            return Ok(examanes);
        }

        // PUT: api/Examanes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putexamanes(int id, examanes examanes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != examanes.id)
            {
                return BadRequest();
            }

            db.Entry(examanes).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!examanesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Examanes
        [ResponseType(typeof(examanes))]
        public IHttpActionResult Postexamanes(examanes examanes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.examanes.Add(examanes);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = examanes.id }, examanes);
        }

        // DELETE: api/Examanes/5
        [ResponseType(typeof(examanes))]
        public IHttpActionResult Deleteexamanes(int id)
        {
            examanes examanes = db.examanes.Find(id);
            if (examanes == null)
            {
                return NotFound();
            }

            db.examanes.Remove(examanes);
            db.SaveChanges();

            return Ok(examanes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool examanesExists(int id)
        {
            return db.examanes.Count(e => e.id == id) > 0;
        }
    }
}