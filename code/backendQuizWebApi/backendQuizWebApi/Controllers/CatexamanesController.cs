﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuizWebApi;

namespace backendQuizWebApi.Controllers
{
    public class CatexamanesController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Catexamanes
        public IQueryable<categorias_examanes> Getcategorias_examanes()
        {
            return db.categorias_examanes;
        }

        // GET: api/Catexamanes/5
        [ResponseType(typeof(categorias_examanes))]
        public IHttpActionResult Getcategorias_examanes(int id)
        {
            categorias_examanes categorias_examanes = db.categorias_examanes.Find(id);
            if (categorias_examanes == null)
            {
                return NotFound();
            }

            return Ok(categorias_examanes);
        }

        // PUT: api/Catexamanes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putcategorias_examanes(int id, categorias_examanes categorias_examanes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categorias_examanes.id)
            {
                return BadRequest();
            }

            db.Entry(categorias_examanes).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!categorias_examanesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Catexamanes
        [ResponseType(typeof(categorias_examanes))]
        public IHttpActionResult Postcategorias_examanes(categorias_examanes categorias_examanes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.categorias_examanes.Add(categorias_examanes);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = categorias_examanes.id }, categorias_examanes);
        }

        // DELETE: api/Catexamanes/5
        [ResponseType(typeof(categorias_examanes))]
        public IHttpActionResult Deletecategorias_examanes(int id)
        {
            categorias_examanes categorias_examanes = db.categorias_examanes.Find(id);
            if (categorias_examanes == null)
            {
                return NotFound();
            }

            db.categorias_examanes.Remove(categorias_examanes);
            db.SaveChanges();

            return Ok(categorias_examanes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool categorias_examanesExists(int id)
        {
            return db.categorias_examanes.Count(e => e.id == id) > 0;
        }
    }
}