﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuizWebApi;

namespace backendQuizWebApi.Controllers
{
    public class ResusuariosController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Resusuarios
        public IQueryable<respuestas_usuarios> Getrespuestas_usuarios()
        {
            return db.respuestas_usuarios;
        }

        // GET: api/Resusuarios/5
        [ResponseType(typeof(respuestas_usuarios))]
        public IHttpActionResult Getrespuestas_usuarios(int id)
        {
            respuestas_usuarios respuestas_usuarios = db.respuestas_usuarios.Find(id);
            if (respuestas_usuarios == null)
            {
                return NotFound();
            }

            return Ok(respuestas_usuarios);
        }

        // PUT: api/Resusuarios/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putrespuestas_usuarios(int id, respuestas_usuarios respuestas_usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != respuestas_usuarios.id)
            {
                return BadRequest();
            }

            db.Entry(respuestas_usuarios).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!respuestas_usuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Resusuarios
        [ResponseType(typeof(respuestas_usuarios))]
        public IHttpActionResult Postrespuestas_usuarios(respuestas_usuarios respuestas_usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.respuestas_usuarios.Add(respuestas_usuarios);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = respuestas_usuarios.id }, respuestas_usuarios);
        }

        // DELETE: api/Resusuarios/5
        [ResponseType(typeof(respuestas_usuarios))]
        public IHttpActionResult Deleterespuestas_usuarios(int id)
        {
            respuestas_usuarios respuestas_usuarios = db.respuestas_usuarios.Find(id);
            if (respuestas_usuarios == null)
            {
                return NotFound();
            }

            db.respuestas_usuarios.Remove(respuestas_usuarios);
            db.SaveChanges();

            return Ok(respuestas_usuarios);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool respuestas_usuariosExists(int id)
        {
            return db.respuestas_usuarios.Count(e => e.id == id) > 0;
        }
    }
}