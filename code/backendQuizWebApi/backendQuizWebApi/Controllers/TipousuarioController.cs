﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuizWebApi;

namespace backendQuizWebApi.Controllers
{
    public class TipousuarioController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Tipousuario
        public IQueryable<tipo_usuarios> Gettipo_usuarios()
        {
            return db.tipo_usuarios;
        }

        // GET: api/Tipousuario/5
        [ResponseType(typeof(tipo_usuarios))]
        public IHttpActionResult Gettipo_usuarios(int id)
        {
            tipo_usuarios tipo_usuarios = db.tipo_usuarios.Find(id);
            if (tipo_usuarios == null)
            {
                return NotFound();
            }

            return Ok(tipo_usuarios);
        }

        // PUT: api/Tipousuario/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttipo_usuarios(int id, tipo_usuarios tipo_usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipo_usuarios.id)
            {
                return BadRequest();
            }

            db.Entry(tipo_usuarios).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tipo_usuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tipousuario
        [ResponseType(typeof(tipo_usuarios))]
        public IHttpActionResult Posttipo_usuarios(tipo_usuarios tipo_usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tipo_usuarios.Add(tipo_usuarios);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipo_usuarios.id }, tipo_usuarios);
        }

        // DELETE: api/Tipousuario/5
        [ResponseType(typeof(tipo_usuarios))]
        public IHttpActionResult Deletetipo_usuarios(int id)
        {
            tipo_usuarios tipo_usuarios = db.tipo_usuarios.Find(id);
            if (tipo_usuarios == null)
            {
                return NotFound();
            }

            db.tipo_usuarios.Remove(tipo_usuarios);
            db.SaveChanges();

            return Ok(tipo_usuarios);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tipo_usuariosExists(int id)
        {
            return db.tipo_usuarios.Count(e => e.id == id) > 0;
        }
    }
}