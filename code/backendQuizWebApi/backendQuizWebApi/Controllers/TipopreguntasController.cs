﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backendQuizWebApi;

namespace backendQuizWebApi.Controllers
{
    public class TipopreguntasController : ApiController
    {
        private fmedina_quizEntities db = new fmedina_quizEntities();

        // GET: api/Tipopreguntas
        public IQueryable<tipo_pregunta> Gettipo_pregunta()
        {
            return db.tipo_pregunta;
        }

        // GET: api/Tipopreguntas/5
        [ResponseType(typeof(tipo_pregunta))]
        public IHttpActionResult Gettipo_pregunta(int id)
        {
            tipo_pregunta tipo_pregunta = db.tipo_pregunta.Find(id);
            if (tipo_pregunta == null)
            {
                return NotFound();
            }

            return Ok(tipo_pregunta);
        }

        // PUT: api/Tipopreguntas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttipo_pregunta(int id, tipo_pregunta tipo_pregunta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipo_pregunta.id)
            {
                return BadRequest();
            }

            db.Entry(tipo_pregunta).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tipo_preguntaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tipopreguntas
        [ResponseType(typeof(tipo_pregunta))]
        public IHttpActionResult Posttipo_pregunta(tipo_pregunta tipo_pregunta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tipo_pregunta.Add(tipo_pregunta);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipo_pregunta.id }, tipo_pregunta);
        }

        // DELETE: api/Tipopreguntas/5
        [ResponseType(typeof(tipo_pregunta))]
        public IHttpActionResult Deletetipo_pregunta(int id)
        {
            tipo_pregunta tipo_pregunta = db.tipo_pregunta.Find(id);
            if (tipo_pregunta == null)
            {
                return NotFound();
            }

            db.tipo_pregunta.Remove(tipo_pregunta);
            db.SaveChanges();

            return Ok(tipo_pregunta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tipo_preguntaExists(int id)
        {
            return db.tipo_pregunta.Count(e => e.id == id) > 0;
        }
    }
}