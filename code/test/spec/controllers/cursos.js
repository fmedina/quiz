'use strict';

describe('Controller: CursosCtrl', function () {

  // load the controller's module
  beforeEach(module('tareas2App'));

  var CursosCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CursosCtrl = $controller('CursosCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
